const character = 'Luigi';
let ninjas: {
    age: number,
    belt: string,
    name: string
};

console.log(character);

const input = document.querySelectorAll('input');
const select = document.querySelector('select');
console.log('input ===== ', input);
console.log('select===== ', select);

input.forEach((inputItem) => {
    console.log('inputItem = ', inputItem)
});

const circle = (diameter: number) => {
    const pi = Math.PI;
    const radius = diameter/2;
    const result = (2*pi)*radius;

    console.log('RESULT =====', result.toFixed(2));
    return result;
}

let FINALRESULT = circle(22);

console.log('FINALRESULT ======', FINALRESULT);

ninjas = {
    age: 20,
    belt: 'red',
    name: 'PERMY', 

}

let mixedNinjaPropTypes: {
    name: any,
    belt: any,
    age: any,
    strength: any
}

mixedNinjaPropTypes = {
    name: 'doooby',
    belt: 20,
    age: false, 
    strength: 'BOOST'
}

console.log('mixedNinjaPropTypes ======', mixedNinjaPropTypes);

let add = (a: number, b: number, c?: number|string) => {
    const answer = a+b
    return `ADDITION Answer ==== ${answer} and value of c is ==== ${c}`;
}

console.log(add(20, 20));
console.log(add(20, 20, 50));

type carType = {
    name: string
    wheels: number,
    doors: number,
    engine: string
}


let carsObject =  {
    name: 'Audi',
    wheels: 4,
    doors: 4,
    engine: '2 Litre'
}


let carTypesItems  = (carsObject: carType) => {
    
    for(let key in carsObject) {
        console.log(`key: ${key} --- value: ${key}`)
    }
}

carTypesItems(carsObject)