import { Invoice } from './classes/Invoices.js';
import { Payment } from './classes/Payment.js';
import { HasFormatter } from './interfaces/HasFormatter.js';
import { ListTemplate } from './classes/ListTemplate.js';

const form = document.querySelector('#itemForm') as HTMLFormElement;
const invoice = document.querySelector('#type') as HTMLSelectElement;
const toFrom = document.querySelector('#tofrom') as HTMLInputElement;
const details = document.querySelector('#details') as HTMLInputElement;
const amount = document.querySelector('#amount') as HTMLInputElement;
const ul = document.querySelector('#itemList') as HTMLUListElement;
const list = new ListTemplate(ul);


const addUid = <T>(obj: T) => {
    let uid: number = Math.floor(Math.random() * 100);
    return {...obj, uid};
}

let appendedObject = addUid({name: 'Permy', age: 40});

console.log('OBJECT = ', appendedObject )

console.log('nameski ===', appendedObject.name)

form.addEventListener('submit', (e: Event) => {
    e.preventDefault();

    let inputValsObj: {
        invoice: string,
        toFrom: string,
        details: string,
        amount: number
    }

    inputValsObj = {
        invoice: invoice.value,
        toFrom: toFrom.value,
        details: details.value,
        amount: amount.valueAsNumber
    }

    let doc: HasFormatter;

    if (inputValsObj.invoice === 'invoice') { 
        doc = new Invoice(inputValsObj.toFrom, inputValsObj.details, inputValsObj.amount) 
    } else {  
        doc = new Payment(inputValsObj.toFrom, inputValsObj.details, inputValsObj.amount)
    }

    list.render(doc, inputValsObj.invoice, 'end');
});
