import { Invoice } from './classes/Invoices.js';
import { Payment } from './classes/Payment.js';
import { ListTemplate } from './classes/ListTemplate.js';
const form = document.querySelector('#itemForm');
const invoice = document.querySelector('#type');
const toFrom = document.querySelector('#tofrom');
const details = document.querySelector('#details');
const amount = document.querySelector('#amount');
const ul = document.querySelector('#itemList');
const list = new ListTemplate(ul);
const addUid = (obj) => {
    let uid = Math.floor(Math.random() * 100);
    return Object.assign(Object.assign({}, obj), { uid });
};
let appendedObject = addUid({ name: 'Permy', age: 40 });
console.log('OBJECT = ', appendedObject);
console.log('nameski ===', appendedObject.name);
form.addEventListener('submit', (e) => {
    e.preventDefault();
    let inputValsObj;
    inputValsObj = {
        invoice: invoice.value,
        toFrom: toFrom.value,
        details: details.value,
        amount: amount.valueAsNumber
    };
    let doc;
    if (inputValsObj.invoice === 'invoice') {
        doc = new Invoice(inputValsObj.toFrom, inputValsObj.details, inputValsObj.amount);
    }
    else {
        doc = new Payment(inputValsObj.toFrom, inputValsObj.details, inputValsObj.amount);
    }
    list.render(doc, inputValsObj.invoice, 'end');
});
