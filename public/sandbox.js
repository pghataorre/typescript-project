"use strict";
const character = 'Luigi';
let ninjas;
console.log(character);
const input = document.querySelectorAll('input');
const select = document.querySelector('select');
console.log('input ===== ', input);
console.log('select===== ', select);
input.forEach((inputItem) => {
    console.log('inputItem = ', inputItem);
});
const circle = (diameter) => {
    const pi = Math.PI;
    const radius = diameter / 2;
    const result = (2 * pi) * radius;
    console.log('RESULT =====', result.toFixed(2));
    return result;
};
let FINALRESULT = circle(22);
console.log('FINALRESULT ======', FINALRESULT);
ninjas = {
    age: 20,
    belt: 'red',
    name: 'PERMY',
};
let mixedNinjaPropTypes;
mixedNinjaPropTypes = {
    name: 'doooby',
    belt: 20,
    age: false,
    strength: 'BOOST'
};
console.log('mixedNinjaPropTypes ======', mixedNinjaPropTypes);
let add = (a, b, c) => {
    const answer = a + b;
    return `ADDITION Answer ==== ${answer} and value of c is ==== ${c}`;
};
console.log(add(20, 20));
console.log(add(20, 20, 50));
let carsObject = {
    name: 'Audi',
    wheels: 4,
    doors: 4,
    engine: '2 Litre'
};
let carTypesItems = (carsObject) => {
    for (let key in carsObject) {
        console.log(`key: ${key} --- value: ${key}`);
    }
};
carTypesItems(carsObject);
